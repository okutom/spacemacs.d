;;; skk-init.el --- SKK init file

;;; Commentary:

;;; Code:

(require 'skk-study)

;; skkで使う文字コードをutf-8にする
(setq skk-ijsyo-code 'utf-8)
;; 注釈を表示
(setq skk-show-annotation t)
;; Enterで改行しない
(setq skk-egg-like-newline t)
;; 対応する括弧を挿入
(setq skk-auto-insert-paren t)
;; 候補バッファの表示
(setq skk-show-candidates-always-pop-to-buffer t)
;; 半角カナを入力
(setq skk-use-jisx0201-input-method t)
;; 送り仮名が厳密に正しい候補を優先
(setq skk-henkan-strict-okuri-precedence t)
;; 辞書の共有
(setq skk-share-private-jisyo t)
;; 辞書に単語が見付からなかった場合にgoogleのapiを使う
(setq skk-use-search-web t)
(when skk-use-search-web
  (add-to-list 'skk-search-prog-list
               '(skk-search-web 'skk-google-cgi-api-for-japanese-input)
               t)
  (setq skk-read-from-minibuffer-function
        (lambda ()
          (char (skk-google-suggest skk-henkan-key))))
  (add-to-list 'skk-completion-prog-list '(skk-comp-google) t))

;; skk-init.el ends here
