#!/bin/sh

if ! type emacs > /dev/null 2>&1; then
    if ! type org.gnu.emacs > /dev/null 2>&1; then
        echo emacs is not installed
        exit
    fi
fi

script_dir=`dirname ${0}`
abs_script_dir=`cd ${script_dir}; pwd`

git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d
(
    cd ~/.emacs.d
    git fetch origin develop
    git checkout develop
)

if [ ${abs_script_dir} != ~/.spacemacs.d ]; then
    ln -snfv ${abs_script_dir} ~/.spacemacs.d
fi

mkdir -p ~/.skk
ln -snfv ~/.spacemacs.d/skk-init.el ~/.skk/init

unset abs_script_dir
unset script_dir

